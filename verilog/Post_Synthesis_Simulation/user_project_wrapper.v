/* Generated by Yosys 0.30+48 (git sha1 14d50a176d5, gcc 8.3.1 -fPIC -Os) */

module user_project_wrapper(wb_clk_i, wb_rst_i, wbs_stb_i, wbs_cyc_i, wbs_we_i, wbs_sel_i, wbs_dat_i, wbs_adr_i, wbs_ack_o, wbs_dat_o, la_data_in, la_data_out, la_oenb, io_in, io_out, io_oeb, user_clock2, user_irq);
  wire _000_;
  wire _001_;
  wire _002_;
  wire _003_;
  wire _004_;
  wire _005_;
  wire _006_;
  wire _007_;
  wire _008_;
  wire _009_;
  wire _010_;
  wire _011_;
  wire _012_;
  wire _013_;
  wire _014_;
  wire _015_;
  wire _016_;
  wire _017_;
  wire _018_;
  wire _019_;
  wire _020_;
  wire _021_;
  wire _022_;
  wire _023_;
  wire _024_;
  wire _025_;
  wire _026_;
  wire _027_;
  wire _028_;
  wire _029_;
  wire _030_;
  wire _031_;
  wire _032_;
  wire _033_;
  wire _034_;
  wire _035_;
  wire _036_;
  wire _037_;
  wire _038_;
  wire _039_;
  wire _040_;
  wire _041_;
  wire _042_;
  wire _043_;
  wire _044_;
  wire _045_;
  wire _046_;
  wire _047_;
  wire _048_;
  wire _049_;
  wire _050_;
  wire _051_;
  wire _052_;
  wire _053_;
  wire _054_;
  wire _055_;
  wire _056_;
  wire _057_;
  wire _058_;
  wire _059_;
  wire _060_;
  wire _061_;
  wire _062_;
  wire _063_;
  wire _064_;
  wire _065_;
  wire _066_;
  wire _067_;
  wire _068_;
  wire _069_;
  wire _070_;
  wire _071_;
  wire _072_;
  wire _073_;
  wire _074_;
  wire _075_;
  wire _076_;
  wire _077_;
  wire _078_;
  wire _079_;
  wire _080_;
  wire _081_;
  wire _082_;
  wire _083_;
  wire _084_;
  wire _085_;
  wire _086_;
  wire _087_;
  wire _088_;
  wire _089_;
  wire _090_;
  wire _091_;
  wire _092_;
  wire _093_;
  wire _094_;
  wire _095_;
  wire _096_;
  wire _097_;
  wire _098_;
  wire _099_;
  wire _100_;
  wire _101_;
  wire _102_;
  wire _103_;
  wire _104_;
  wire _105_;
  wire _106_;
  wire _107_;
  wire _108_;
  wire _109_;
  wire _110_;
  wire _111_;
  wire _112_;
  wire _113_;
  wire _114_;
  wire _115_;
  wire _116_;
  wire _117_;
  wire _118_;
  wire _119_;
  wire _120_;
  wire _121_;
  wire _122_;
  wire _123_;
  wire _124_;
  wire _125_;
  wire _126_;
  wire _127_;
  wire _128_;
  wire _129_;
  wire _130_;
  wire _131_;
  wire _132_;
  wire _133_;
  wire _134_;
  wire _135_;
  wire _136_;
  wire _137_;
  wire _138_;
  wire _139_;
  wire _140_;
  wire _141_;
  wire _142_;
  wire _143_;
  wire _144_;
  wire _145_;
  wire _146_;
  wire _147_;
  wire _148_;
  wire _149_;
  wire _150_;
  wire _151_;
  wire _152_;
  wire _153_;
  wire _154_;
  wire _155_;
  wire _156_;
  wire _157_;
  wire _158_;
  wire _159_;
  wire _160_;
  wire _161_;
  wire _162_;
  wire _163_;
  wire _164_;
  wire _165_;
  wire _166_;
  wire _167_;
  input [37:0] io_in;
  wire [37:0] io_in;
  output [37:0] io_oeb;
  wire [37:0] io_oeb;
  output [37:0] io_out;
  wire [37:0] io_out;
  input [63:0] la_data_in;
  wire [63:0] la_data_in;
  output [63:0] la_data_out;
  wire [63:0] la_data_out;
  input [63:0] la_oenb;
  wire [63:0] la_oenb;
  input user_clock2;
  wire user_clock2;
  output [2:0] user_irq;
  wire [2:0] user_irq;
  wire valid;
  input wb_clk_i;
  wire wb_clk_i;
  input wb_rst_i;
  wire wb_rst_i;
  output wbs_ack_o;
  wire wbs_ack_o;
  wire wbs_ack_o_wbuart;
  input [31:0] wbs_adr_i;
  wire [31:0] wbs_adr_i;
  wire \wbs_adr_i_wbuart[10] ;
  wire \wbs_adr_i_wbuart[9] ;
  wire \wbs_adr_i_xtea[0] ;
  wire \wbs_adr_i_xtea[1] ;
  wire \wbs_adr_i_xtea[2] ;
  wire \wbs_adr_i_xtea[3] ;
  wire \wbs_adr_i_xtea[4] ;
  wire \wbs_adr_i_xtea[5] ;
  wire \wbs_adr_i_xtea[6] ;
  wire \wbs_adr_i_xtea[7] ;
  input wbs_cyc_i;
  wire wbs_cyc_i;
  input [31:0] wbs_dat_i;
  wire [31:0] wbs_dat_i;
  output [31:0] wbs_dat_o;
  wire [31:0] wbs_dat_o;
  wire \wbs_dat_o_wbuart[0] ;
  wire \wbs_dat_o_wbuart[10] ;
  wire \wbs_dat_o_wbuart[11] ;
  wire \wbs_dat_o_wbuart[12] ;
  wire \wbs_dat_o_wbuart[13] ;
  wire \wbs_dat_o_wbuart[14] ;
  wire \wbs_dat_o_wbuart[15] ;
  wire \wbs_dat_o_wbuart[16] ;
  wire \wbs_dat_o_wbuart[17] ;
  wire \wbs_dat_o_wbuart[18] ;
  wire \wbs_dat_o_wbuart[19] ;
  wire \wbs_dat_o_wbuart[1] ;
  wire \wbs_dat_o_wbuart[20] ;
  wire \wbs_dat_o_wbuart[21] ;
  wire \wbs_dat_o_wbuart[22] ;
  wire \wbs_dat_o_wbuart[23] ;
  wire \wbs_dat_o_wbuart[24] ;
  wire \wbs_dat_o_wbuart[25] ;
  wire \wbs_dat_o_wbuart[26] ;
  wire \wbs_dat_o_wbuart[27] ;
  wire \wbs_dat_o_wbuart[28] ;
  wire \wbs_dat_o_wbuart[29] ;
  wire \wbs_dat_o_wbuart[2] ;
  wire \wbs_dat_o_wbuart[30] ;
  wire \wbs_dat_o_wbuart[31] ;
  wire \wbs_dat_o_wbuart[3] ;
  wire \wbs_dat_o_wbuart[4] ;
  wire \wbs_dat_o_wbuart[5] ;
  wire \wbs_dat_o_wbuart[6] ;
  wire \wbs_dat_o_wbuart[7] ;
  wire \wbs_dat_o_wbuart[8] ;
  wire \wbs_dat_o_wbuart[9] ;
  wire \wbs_dat_o_xtea[0] ;
  wire \wbs_dat_o_xtea[10] ;
  wire \wbs_dat_o_xtea[11] ;
  wire \wbs_dat_o_xtea[12] ;
  wire \wbs_dat_o_xtea[13] ;
  wire \wbs_dat_o_xtea[14] ;
  wire \wbs_dat_o_xtea[15] ;
  wire \wbs_dat_o_xtea[16] ;
  wire \wbs_dat_o_xtea[17] ;
  wire \wbs_dat_o_xtea[18] ;
  wire \wbs_dat_o_xtea[19] ;
  wire \wbs_dat_o_xtea[1] ;
  wire \wbs_dat_o_xtea[20] ;
  wire \wbs_dat_o_xtea[21] ;
  wire \wbs_dat_o_xtea[22] ;
  wire \wbs_dat_o_xtea[23] ;
  wire \wbs_dat_o_xtea[24] ;
  wire \wbs_dat_o_xtea[25] ;
  wire \wbs_dat_o_xtea[26] ;
  wire \wbs_dat_o_xtea[27] ;
  wire \wbs_dat_o_xtea[28] ;
  wire \wbs_dat_o_xtea[29] ;
  wire \wbs_dat_o_xtea[2] ;
  wire \wbs_dat_o_xtea[30] ;
  wire \wbs_dat_o_xtea[31] ;
  wire \wbs_dat_o_xtea[3] ;
  wire \wbs_dat_o_xtea[4] ;
  wire \wbs_dat_o_xtea[5] ;
  wire \wbs_dat_o_xtea[6] ;
  wire \wbs_dat_o_xtea[7] ;
  wire \wbs_dat_o_xtea[8] ;
  wire \wbs_dat_o_xtea[9] ;
  input [3:0] wbs_sel_i;
  wire [3:0] wbs_sel_i;
  input wbs_stb_i;
  wire wbs_stb_i;
  input wbs_we_i;
  wire wbs_we_i;
  gf180mcu_fd_sc_mcu7t5v0__and2_1 _168_ (
    .A1(wbs_cyc_i),
    .A2(wbs_stb_i),
    .Z(_043_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _169_ (
    .I(_043_),
    .Z(valid)
  );
  gf180mcu_fd_sc_mcu7t5v0__clkinv_1 _170_ (
    .I(wbs_adr_i[14]),
    .ZN(_044_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _171_ (
    .I(_044_),
    .Z(_045_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _172_ (
    .I(wbs_adr_i[13]),
    .Z(_046_)
  );
  gf180mcu_fd_sc_mcu7t5v0__clkinv_1 _173_ (
    .I(wbs_adr_i[15]),
    .ZN(_047_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _174_ (
    .I(_047_),
    .Z(_048_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _175_ (
    .A1(_045_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[0] ),
    .ZN(_049_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _176_ (
    .I(wbs_adr_i[14]),
    .Z(_050_)
  );
  gf180mcu_fd_sc_mcu7t5v0__clkinv_1 _177_ (
    .I(wbs_adr_i[13]),
    .ZN(_051_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _178_ (
    .I(_051_),
    .Z(_052_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _179_ (
    .I(wbs_adr_i[15]),
    .Z(_053_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _180_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[0] ),
    .ZN(_054_)
  );
  gf180mcu_fd_sc_mcu7t5v0__clkbuf_1 _181_ (
    .I(wb_rst_i),
    .Z(_055_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _182_ (
    .I(_055_),
    .Z(_056_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _183_ (
    .A1(_049_),
    .A2(_054_),
    .B(_056_),
    .ZN(_000_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _184_ (
    .I(_044_),
    .Z(_057_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _185_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[1] ),
    .ZN(_058_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _186_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[1] ),
    .ZN(_059_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _187_ (
    .A1(_058_),
    .A2(_059_),
    .B(_056_),
    .ZN(_001_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _188_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[2] ),
    .ZN(_060_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _189_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[2] ),
    .ZN(_061_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _190_ (
    .A1(_060_),
    .A2(_061_),
    .B(_056_),
    .ZN(_002_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _191_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[3] ),
    .ZN(_062_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _192_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[3] ),
    .ZN(_063_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _193_ (
    .A1(_062_),
    .A2(_063_),
    .B(_056_),
    .ZN(_003_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _194_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[4] ),
    .ZN(_064_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _195_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[4] ),
    .ZN(_065_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _196_ (
    .A1(_064_),
    .A2(_065_),
    .B(_056_),
    .ZN(_004_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _197_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[5] ),
    .ZN(_066_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _198_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[5] ),
    .ZN(_067_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _199_ (
    .A1(_066_),
    .A2(_067_),
    .B(_056_),
    .ZN(_005_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _200_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[6] ),
    .ZN(_068_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _201_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[6] ),
    .ZN(_069_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _202_ (
    .A1(_068_),
    .A2(_069_),
    .B(_056_),
    .ZN(_006_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _203_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[7] ),
    .ZN(_070_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _204_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[7] ),
    .ZN(_071_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _205_ (
    .A1(_070_),
    .A2(_071_),
    .B(_056_),
    .ZN(_007_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _206_ (
    .A1(_057_),
    .A2(_046_),
    .A3(_048_),
    .A4(\wbs_dat_o_xtea[8] ),
    .ZN(_072_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _207_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[8] ),
    .ZN(_073_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _208_ (
    .A1(_072_),
    .A2(_073_),
    .B(_056_),
    .ZN(_008_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _209_ (
    .I(wbs_adr_i[13]),
    .Z(_074_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _210_ (
    .I(_047_),
    .Z(_075_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _211_ (
    .A1(_057_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[9] ),
    .ZN(_076_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _212_ (
    .A1(_050_),
    .A2(_052_),
    .A3(_053_),
    .A4(\wbs_dat_o_wbuart[9] ),
    .ZN(_077_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _213_ (
    .A1(_076_),
    .A2(_077_),
    .B(_056_),
    .ZN(_009_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _214_ (
    .A1(_057_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[10] ),
    .ZN(_078_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _215_ (
    .I(wbs_adr_i[14]),
    .Z(_079_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _216_ (
    .I(_051_),
    .Z(_080_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _217_ (
    .I(wbs_adr_i[15]),
    .Z(_081_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _218_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[10] ),
    .ZN(_082_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _219_ (
    .I(_055_),
    .Z(_083_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _220_ (
    .A1(_078_),
    .A2(_082_),
    .B(_083_),
    .ZN(_010_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _221_ (
    .I(_044_),
    .Z(_084_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _222_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[11] ),
    .ZN(_085_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _223_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[11] ),
    .ZN(_086_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _224_ (
    .A1(_085_),
    .A2(_086_),
    .B(_083_),
    .ZN(_011_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _225_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[12] ),
    .ZN(_087_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _226_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[12] ),
    .ZN(_088_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _227_ (
    .A1(_087_),
    .A2(_088_),
    .B(_083_),
    .ZN(_012_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _228_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[13] ),
    .ZN(_089_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _229_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[13] ),
    .ZN(_090_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _230_ (
    .A1(_089_),
    .A2(_090_),
    .B(_083_),
    .ZN(_013_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _231_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[14] ),
    .ZN(_091_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _232_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[14] ),
    .ZN(_092_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _233_ (
    .A1(_091_),
    .A2(_092_),
    .B(_083_),
    .ZN(_014_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _234_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[15] ),
    .ZN(_093_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _235_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[15] ),
    .ZN(_094_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _236_ (
    .A1(_093_),
    .A2(_094_),
    .B(_083_),
    .ZN(_015_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _237_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[16] ),
    .ZN(_095_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _238_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[16] ),
    .ZN(_096_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _239_ (
    .A1(_095_),
    .A2(_096_),
    .B(_083_),
    .ZN(_016_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _240_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[17] ),
    .ZN(_097_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _241_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[17] ),
    .ZN(_098_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _242_ (
    .A1(_097_),
    .A2(_098_),
    .B(_083_),
    .ZN(_017_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _243_ (
    .A1(_084_),
    .A2(_074_),
    .A3(_075_),
    .A4(\wbs_dat_o_xtea[18] ),
    .ZN(_099_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _244_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[18] ),
    .ZN(_100_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _245_ (
    .A1(_099_),
    .A2(_100_),
    .B(_083_),
    .ZN(_018_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _246_ (
    .I(wbs_adr_i[13]),
    .Z(_101_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _247_ (
    .I(_047_),
    .Z(_102_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _248_ (
    .A1(_084_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[19] ),
    .ZN(_103_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _249_ (
    .A1(_079_),
    .A2(_080_),
    .A3(_081_),
    .A4(\wbs_dat_o_wbuart[19] ),
    .ZN(_104_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _250_ (
    .A1(_103_),
    .A2(_104_),
    .B(_083_),
    .ZN(_019_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _251_ (
    .A1(_084_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[20] ),
    .ZN(_105_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _252_ (
    .I(wbs_adr_i[14]),
    .Z(_106_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _253_ (
    .I(_051_),
    .Z(_107_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _254_ (
    .I(wbs_adr_i[15]),
    .Z(_108_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _255_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[20] ),
    .ZN(_109_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _256_ (
    .I(_055_),
    .Z(_110_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _257_ (
    .A1(_105_),
    .A2(_109_),
    .B(_110_),
    .ZN(_020_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _258_ (
    .I(_044_),
    .Z(_111_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _259_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[21] ),
    .ZN(_112_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _260_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[21] ),
    .ZN(_113_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _261_ (
    .A1(_112_),
    .A2(_113_),
    .B(_110_),
    .ZN(_021_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _262_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[22] ),
    .ZN(_114_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _263_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[22] ),
    .ZN(_115_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _264_ (
    .A1(_114_),
    .A2(_115_),
    .B(_110_),
    .ZN(_022_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _265_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[23] ),
    .ZN(_116_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _266_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[23] ),
    .ZN(_117_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _267_ (
    .A1(_116_),
    .A2(_117_),
    .B(_110_),
    .ZN(_023_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _268_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[24] ),
    .ZN(_118_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _269_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[24] ),
    .ZN(_119_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _270_ (
    .A1(_118_),
    .A2(_119_),
    .B(_110_),
    .ZN(_024_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _271_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[25] ),
    .ZN(_120_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _272_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[25] ),
    .ZN(_121_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _273_ (
    .A1(_120_),
    .A2(_121_),
    .B(_110_),
    .ZN(_025_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _274_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[26] ),
    .ZN(_122_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _275_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[26] ),
    .ZN(_123_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _276_ (
    .A1(_122_),
    .A2(_123_),
    .B(_110_),
    .ZN(_026_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _277_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[27] ),
    .ZN(_124_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _278_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[27] ),
    .ZN(_125_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _279_ (
    .A1(_124_),
    .A2(_125_),
    .B(_110_),
    .ZN(_027_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _280_ (
    .A1(_111_),
    .A2(_101_),
    .A3(_102_),
    .A4(\wbs_dat_o_xtea[28] ),
    .ZN(_126_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _281_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[28] ),
    .ZN(_127_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _282_ (
    .A1(_126_),
    .A2(_127_),
    .B(_110_),
    .ZN(_028_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _283_ (
    .A1(_111_),
    .A2(wbs_adr_i[13]),
    .A3(_047_),
    .A4(\wbs_dat_o_xtea[29] ),
    .ZN(_128_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _284_ (
    .A1(_106_),
    .A2(_107_),
    .A3(_108_),
    .A4(\wbs_dat_o_wbuart[29] ),
    .ZN(_129_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _285_ (
    .A1(_128_),
    .A2(_129_),
    .B(_110_),
    .ZN(_029_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _286_ (
    .A1(_111_),
    .A2(wbs_adr_i[13]),
    .A3(_047_),
    .A4(\wbs_dat_o_xtea[30] ),
    .ZN(_130_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _287_ (
    .I(wbs_adr_i[14]),
    .Z(_131_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _288_ (
    .I(_051_),
    .Z(_132_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _289_ (
    .I(wbs_adr_i[15]),
    .Z(_133_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _290_ (
    .A1(_131_),
    .A2(_132_),
    .A3(_133_),
    .A4(\wbs_dat_o_wbuart[30] ),
    .ZN(_134_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _291_ (
    .I(_055_),
    .Z(_135_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _292_ (
    .A1(_130_),
    .A2(_134_),
    .B(_135_),
    .ZN(_030_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _293_ (
    .A1(_044_),
    .A2(wbs_adr_i[13]),
    .A3(_047_),
    .A4(\wbs_dat_o_xtea[31] ),
    .ZN(_136_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _294_ (
    .A1(_131_),
    .A2(_132_),
    .A3(_133_),
    .A4(\wbs_dat_o_wbuart[31] ),
    .ZN(_137_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _295_ (
    .A1(_136_),
    .A2(_137_),
    .B(_135_),
    .ZN(_031_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _296_ (
    .A1(\wbs_adr_i_xtea[0] ),
    .A2(_131_),
    .A3(_132_),
    .A4(_133_),
    .ZN(_138_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _297_ (
    .I(wbs_adr_i[13]),
    .Z(_139_)
  );
  gf180mcu_fd_sc_mcu7t5v0__buf_1 _298_ (
    .I(_047_),
    .Z(_140_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _299_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[0]),
    .ZN(_141_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _300_ (
    .A1(_138_),
    .A2(_141_),
    .B(_135_),
    .ZN(_032_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _301_ (
    .A1(\wbs_adr_i_xtea[1] ),
    .A2(_131_),
    .A3(_132_),
    .A4(_133_),
    .ZN(_142_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _302_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[1]),
    .ZN(_143_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _303_ (
    .A1(_142_),
    .A2(_143_),
    .B(_135_),
    .ZN(_033_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _304_ (
    .A1(\wbs_adr_i_xtea[2] ),
    .A2(_131_),
    .A3(_132_),
    .A4(_133_),
    .ZN(_144_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _305_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[2]),
    .ZN(_145_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _306_ (
    .A1(_144_),
    .A2(_145_),
    .B(_135_),
    .ZN(_034_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _307_ (
    .A1(\wbs_adr_i_xtea[3] ),
    .A2(_131_),
    .A3(_132_),
    .A4(_133_),
    .ZN(_146_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _308_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[3]),
    .ZN(_147_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _309_ (
    .A1(_146_),
    .A2(_147_),
    .B(_135_),
    .ZN(_035_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _310_ (
    .A1(\wbs_adr_i_xtea[4] ),
    .A2(_131_),
    .A3(_132_),
    .A4(_133_),
    .ZN(_148_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _311_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[4]),
    .ZN(_149_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _312_ (
    .A1(_148_),
    .A2(_149_),
    .B(_135_),
    .ZN(_036_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _313_ (
    .A1(\wbs_adr_i_xtea[5] ),
    .A2(wbs_adr_i[14]),
    .A3(_051_),
    .A4(wbs_adr_i[15]),
    .ZN(_150_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _314_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[5]),
    .ZN(_151_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _315_ (
    .A1(_150_),
    .A2(_151_),
    .B(_135_),
    .ZN(_037_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _316_ (
    .A1(\wbs_adr_i_xtea[6] ),
    .A2(wbs_adr_i[14]),
    .A3(_051_),
    .A4(wbs_adr_i[15]),
    .ZN(_152_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _317_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[6]),
    .ZN(_153_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _318_ (
    .A1(_152_),
    .A2(_153_),
    .B(_135_),
    .ZN(_038_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _319_ (
    .A1(\wbs_adr_i_xtea[7] ),
    .A2(wbs_adr_i[14]),
    .A3(_051_),
    .A4(wbs_adr_i[15]),
    .ZN(_154_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _320_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .A4(wbs_adr_i[7]),
    .ZN(_155_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _321_ (
    .A1(_154_),
    .A2(_155_),
    .B(_135_),
    .ZN(_039_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _322_ (
    .A1(\wbs_adr_i_wbuart[9] ),
    .A2(_044_),
    .A3(_139_),
    .A4(_140_),
    .ZN(_156_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _323_ (
    .A1(_131_),
    .A2(_132_),
    .A3(_133_),
    .A4(wbs_adr_i[9]),
    .ZN(_157_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _324_ (
    .A1(_156_),
    .A2(_157_),
    .B(_055_),
    .ZN(_040_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _325_ (
    .A1(\wbs_adr_i_wbuart[10] ),
    .A2(_044_),
    .A3(_046_),
    .A4(_048_),
    .ZN(_158_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _326_ (
    .A1(_131_),
    .A2(_132_),
    .A3(_133_),
    .A4(wbs_adr_i[10]),
    .ZN(_159_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _327_ (
    .A1(_158_),
    .A2(_159_),
    .B(_055_),
    .ZN(_041_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand3_1 _328_ (
    .A1(_045_),
    .A2(_139_),
    .A3(_140_),
    .ZN(_160_)
  );
  gf180mcu_fd_sc_mcu7t5v0__nand4_1 _329_ (
    .A1(_131_),
    .A2(_132_),
    .A3(_133_),
    .A4(wbs_ack_o_wbuart),
    .ZN(_161_)
  );
  gf180mcu_fd_sc_mcu7t5v0__aoi21_1 _330_ (
    .A1(_160_),
    .A2(_161_),
    .B(_055_),
    .ZN(_042_)
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _331_ (
    .CLK(wb_clk_i),
    .D(_000_),
    .Q(wbs_dat_o[0])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _332_ (
    .CLK(wb_clk_i),
    .D(_001_),
    .Q(wbs_dat_o[1])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _333_ (
    .CLK(wb_clk_i),
    .D(_002_),
    .Q(wbs_dat_o[2])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _334_ (
    .CLK(wb_clk_i),
    .D(_003_),
    .Q(wbs_dat_o[3])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _335_ (
    .CLK(wb_clk_i),
    .D(_004_),
    .Q(wbs_dat_o[4])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _336_ (
    .CLK(wb_clk_i),
    .D(_005_),
    .Q(wbs_dat_o[5])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _337_ (
    .CLK(wb_clk_i),
    .D(_006_),
    .Q(wbs_dat_o[6])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _338_ (
    .CLK(wb_clk_i),
    .D(_007_),
    .Q(wbs_dat_o[7])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _339_ (
    .CLK(wb_clk_i),
    .D(_008_),
    .Q(wbs_dat_o[8])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _340_ (
    .CLK(wb_clk_i),
    .D(_009_),
    .Q(wbs_dat_o[9])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _341_ (
    .CLK(wb_clk_i),
    .D(_010_),
    .Q(wbs_dat_o[10])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _342_ (
    .CLK(wb_clk_i),
    .D(_011_),
    .Q(wbs_dat_o[11])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _343_ (
    .CLK(wb_clk_i),
    .D(_012_),
    .Q(wbs_dat_o[12])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _344_ (
    .CLK(wb_clk_i),
    .D(_013_),
    .Q(wbs_dat_o[13])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _345_ (
    .CLK(wb_clk_i),
    .D(_014_),
    .Q(wbs_dat_o[14])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _346_ (
    .CLK(wb_clk_i),
    .D(_015_),
    .Q(wbs_dat_o[15])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _347_ (
    .CLK(wb_clk_i),
    .D(_016_),
    .Q(wbs_dat_o[16])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _348_ (
    .CLK(wb_clk_i),
    .D(_017_),
    .Q(wbs_dat_o[17])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _349_ (
    .CLK(wb_clk_i),
    .D(_018_),
    .Q(wbs_dat_o[18])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _350_ (
    .CLK(wb_clk_i),
    .D(_019_),
    .Q(wbs_dat_o[19])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _351_ (
    .CLK(wb_clk_i),
    .D(_020_),
    .Q(wbs_dat_o[20])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _352_ (
    .CLK(wb_clk_i),
    .D(_021_),
    .Q(wbs_dat_o[21])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _353_ (
    .CLK(wb_clk_i),
    .D(_022_),
    .Q(wbs_dat_o[22])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _354_ (
    .CLK(wb_clk_i),
    .D(_023_),
    .Q(wbs_dat_o[23])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _355_ (
    .CLK(wb_clk_i),
    .D(_024_),
    .Q(wbs_dat_o[24])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _356_ (
    .CLK(wb_clk_i),
    .D(_025_),
    .Q(wbs_dat_o[25])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _357_ (
    .CLK(wb_clk_i),
    .D(_026_),
    .Q(wbs_dat_o[26])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _358_ (
    .CLK(wb_clk_i),
    .D(_027_),
    .Q(wbs_dat_o[27])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _359_ (
    .CLK(wb_clk_i),
    .D(_028_),
    .Q(wbs_dat_o[28])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _360_ (
    .CLK(wb_clk_i),
    .D(_029_),
    .Q(wbs_dat_o[29])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _361_ (
    .CLK(wb_clk_i),
    .D(_030_),
    .Q(wbs_dat_o[30])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _362_ (
    .CLK(wb_clk_i),
    .D(_031_),
    .Q(wbs_dat_o[31])
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _363_ (
    .CLK(wb_clk_i),
    .D(_032_),
    .Q(\wbs_adr_i_xtea[0] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _364_ (
    .CLK(wb_clk_i),
    .D(_033_),
    .Q(\wbs_adr_i_xtea[1] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _365_ (
    .CLK(wb_clk_i),
    .D(_034_),
    .Q(\wbs_adr_i_xtea[2] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _366_ (
    .CLK(wb_clk_i),
    .D(_035_),
    .Q(\wbs_adr_i_xtea[3] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _367_ (
    .CLK(wb_clk_i),
    .D(_036_),
    .Q(\wbs_adr_i_xtea[4] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _368_ (
    .CLK(wb_clk_i),
    .D(_037_),
    .Q(\wbs_adr_i_xtea[5] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _369_ (
    .CLK(wb_clk_i),
    .D(_038_),
    .Q(\wbs_adr_i_xtea[6] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _370_ (
    .CLK(wb_clk_i),
    .D(_039_),
    .Q(\wbs_adr_i_xtea[7] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _371_ (
    .CLK(wb_clk_i),
    .D(_040_),
    .Q(\wbs_adr_i_wbuart[9] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _372_ (
    .CLK(wb_clk_i),
    .D(_041_),
    .Q(\wbs_adr_i_wbuart[10] )
  );
  gf180mcu_fd_sc_mcu7t5v0__dffq_1 _373_ (
    .CLK(wb_clk_i),
    .D(_042_),
    .Q(wbs_ack_o)
  );
  gf180mcu_fd_sc_mcu7t5v0__tieh _374_ (
    .Z(io_oeb[36])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _375_ (
    .ZN(_166_)
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _376_ (
    .ZN(_167_)
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _377_ (
    .ZN(io_oeb[37])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _378_ (
    .ZN(io_out[36])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _379_ (
    .ZN(la_data_out[0])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _380_ (
    .ZN(la_data_out[1])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _381_ (
    .ZN(la_data_out[2])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _382_ (
    .ZN(la_data_out[3])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _383_ (
    .ZN(la_data_out[4])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _384_ (
    .ZN(la_data_out[5])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _385_ (
    .ZN(la_data_out[6])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _386_ (
    .ZN(la_data_out[7])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _387_ (
    .ZN(la_data_out[8])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _388_ (
    .ZN(la_data_out[9])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _389_ (
    .ZN(la_data_out[10])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _390_ (
    .ZN(la_data_out[11])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _391_ (
    .ZN(la_data_out[12])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _392_ (
    .ZN(la_data_out[13])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _393_ (
    .ZN(la_data_out[14])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _394_ (
    .ZN(la_data_out[15])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _395_ (
    .ZN(la_data_out[16])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _396_ (
    .ZN(la_data_out[17])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _397_ (
    .ZN(la_data_out[18])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _398_ (
    .ZN(la_data_out[19])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _399_ (
    .ZN(la_data_out[20])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _400_ (
    .ZN(la_data_out[21])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _401_ (
    .ZN(la_data_out[22])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _402_ (
    .ZN(la_data_out[23])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _403_ (
    .ZN(la_data_out[24])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _404_ (
    .ZN(la_data_out[25])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _405_ (
    .ZN(la_data_out[26])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _406_ (
    .ZN(la_data_out[27])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _407_ (
    .ZN(la_data_out[28])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _408_ (
    .ZN(la_data_out[29])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _409_ (
    .ZN(la_data_out[30])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _410_ (
    .ZN(la_data_out[31])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _411_ (
    .ZN(la_data_out[32])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _412_ (
    .ZN(la_data_out[33])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _413_ (
    .ZN(la_data_out[34])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _414_ (
    .ZN(la_data_out[35])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _415_ (
    .ZN(la_data_out[36])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _416_ (
    .ZN(la_data_out[37])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _417_ (
    .ZN(la_data_out[38])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _418_ (
    .ZN(la_data_out[39])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _419_ (
    .ZN(la_data_out[40])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _420_ (
    .ZN(la_data_out[41])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _421_ (
    .ZN(la_data_out[42])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _422_ (
    .ZN(la_data_out[43])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _423_ (
    .ZN(la_data_out[44])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _424_ (
    .ZN(la_data_out[45])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _425_ (
    .ZN(la_data_out[46])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _426_ (
    .ZN(la_data_out[47])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _427_ (
    .ZN(la_data_out[48])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _428_ (
    .ZN(la_data_out[49])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _429_ (
    .ZN(la_data_out[50])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _430_ (
    .ZN(la_data_out[51])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _431_ (
    .ZN(la_data_out[52])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _432_ (
    .ZN(la_data_out[53])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _433_ (
    .ZN(la_data_out[54])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _434_ (
    .ZN(la_data_out[55])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _435_ (
    .ZN(la_data_out[56])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _436_ (
    .ZN(la_data_out[57])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _437_ (
    .ZN(la_data_out[58])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _438_ (
    .ZN(la_data_out[59])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _439_ (
    .ZN(la_data_out[60])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _440_ (
    .ZN(la_data_out[61])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _441_ (
    .ZN(la_data_out[62])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _442_ (
    .ZN(la_data_out[63])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _443_ (
    .ZN(user_irq[0])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _444_ (
    .ZN(user_irq[1])
  );
  gf180mcu_fd_sc_mcu7t5v0__tiel _445_ (
    .ZN(user_irq[2])
  );
  wbuart wbuart (
    .i_clk(wb_clk_i),
    .i_reset(wb_rst_i),
    .i_uart_rx(io_in[36]),
    .i_wb_addr({ \wbs_adr_i_wbuart[10] , \wbs_adr_i_wbuart[9]  }),
    .i_wb_cyc(wbs_cyc_i),
    .i_wb_data(wbs_dat_i),
    .i_wb_sel(wbs_sel_i),
    .i_wb_stb(wbs_stb_i),
    .i_wb_we(wbs_we_i),
    .o_uart_tx(io_out[37]),
    .o_wb_ack(wbs_ack_o_wbuart),
    .o_wb_data({ \wbs_dat_o_wbuart[31] , \wbs_dat_o_wbuart[30] , \wbs_dat_o_wbuart[29] , \wbs_dat_o_wbuart[28] , \wbs_dat_o_wbuart[27] , \wbs_dat_o_wbuart[26] , \wbs_dat_o_wbuart[25] , \wbs_dat_o_wbuart[24] , \wbs_dat_o_wbuart[23] , \wbs_dat_o_wbuart[22] , \wbs_dat_o_wbuart[21] , \wbs_dat_o_wbuart[20] , \wbs_dat_o_wbuart[19] , \wbs_dat_o_wbuart[18] , \wbs_dat_o_wbuart[17] , \wbs_dat_o_wbuart[16] , \wbs_dat_o_wbuart[15] , \wbs_dat_o_wbuart[14] , \wbs_dat_o_wbuart[13] , \wbs_dat_o_wbuart[12] , \wbs_dat_o_wbuart[11] , \wbs_dat_o_wbuart[10] , \wbs_dat_o_wbuart[9] , \wbs_dat_o_wbuart[8] , \wbs_dat_o_wbuart[7] , \wbs_dat_o_wbuart[6] , \wbs_dat_o_wbuart[5] , \wbs_dat_o_wbuart[4] , \wbs_dat_o_wbuart[3] , \wbs_dat_o_wbuart[2] , \wbs_dat_o_wbuart[1] , \wbs_dat_o_wbuart[0]  })
  );
  wrapped_design wrapped_design (
    .io_in({ _167_, _166_, io_in[35:0] }),
    .io_oeb({ _163_, _162_, io_oeb[35:0] }),
    .io_out({ _165_, _164_, io_out[35:0] }),
    .wb_clk_i(wb_clk_i),
    .wb_rst_i(wb_rst_i)
  );
  xtea xtea (
    .address({ \wbs_adr_i_xtea[7] , \wbs_adr_i_xtea[6] , \wbs_adr_i_xtea[5] , \wbs_adr_i_xtea[4] , \wbs_adr_i_xtea[3] , \wbs_adr_i_xtea[2] , \wbs_adr_i_xtea[1] , \wbs_adr_i_xtea[0]  }),
    .clk(wb_clk_i),
    .cs(valid),
    .read_data({ \wbs_dat_o_xtea[31] , \wbs_dat_o_xtea[30] , \wbs_dat_o_xtea[29] , \wbs_dat_o_xtea[28] , \wbs_dat_o_xtea[27] , \wbs_dat_o_xtea[26] , \wbs_dat_o_xtea[25] , \wbs_dat_o_xtea[24] , \wbs_dat_o_xtea[23] , \wbs_dat_o_xtea[22] , \wbs_dat_o_xtea[21] , \wbs_dat_o_xtea[20] , \wbs_dat_o_xtea[19] , \wbs_dat_o_xtea[18] , \wbs_dat_o_xtea[17] , \wbs_dat_o_xtea[16] , \wbs_dat_o_xtea[15] , \wbs_dat_o_xtea[14] , \wbs_dat_o_xtea[13] , \wbs_dat_o_xtea[12] , \wbs_dat_o_xtea[11] , \wbs_dat_o_xtea[10] , \wbs_dat_o_xtea[9] , \wbs_dat_o_xtea[8] , \wbs_dat_o_xtea[7] , \wbs_dat_o_xtea[6] , \wbs_dat_o_xtea[5] , \wbs_dat_o_xtea[4] , \wbs_dat_o_xtea[3] , \wbs_dat_o_xtea[2] , \wbs_dat_o_xtea[1] , \wbs_dat_o_xtea[0]  }),
    .reset_n(wb_rst_i),
    .we(wbs_we_i),
    .write_data(wbs_dat_i)
  );
endmodule
