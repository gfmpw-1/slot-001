// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0



`default_nettype none
// update this to the name of your module
module wrapped_design(

`ifdef USE_POWER_PINS
    inout vdd,	// User area 1 1.8V supply
    inout vss,	// User area 1 digital ground
`endif

    // interface as user_proj_example.v
    input wire wb_clk_i,
    input wire wb_rst_i,

    // Logic Analyzer Signals
    // only provide first 32 bits to reduce wiring congestion

    input  wire [35:0] io_in,
    output wire [35:0] io_out,
    output wire [35:0] io_oeb

);

    // permanently set oeb so that outputs are always enabled: 0 is output, 1 is high-impedance
   
   //assign io_oeb = {`MPRJ_IO_PADS{1'b0}};
 
   // Not used

    assign io_out[4:0] = 5'b00000;
    assign io_oeb[4:0] = 5'b11111;

   // Used
   
    assign io_oeb[7:5]  = 3'b111;
    assign io_out[7:5]  = 3'b000;
    
    assign io_oeb[12:8]  = 5'b11111;
    assign io_out[12:8]  = 5'b00000;
    
    assign io_oeb[20:13] = 8'b00000000;
    
    assign io_oeb[21] = 1'b0;
   
    
    assign io_oeb[35:22] = 14'b00000000000000;
    
   
   
   // 
   // assign io_oeb[8:5] = 4'b0100;
   // assign io_out[7] = 1'b0;
   // SPI signals
   // .sck        (io_out[5]),
   // .sdo        (io_out[6]),
   // .sdi        (io_in[7]),
   // .cs         (io_out[8])

  
top_vga top_vga(
 
 	.clk(wb_clk_i),   
 	
 //	rest box and clk	              
 	.reset_box(io_in[8]),
	.reset_n(io_in[9]),
	
// 	inputs for clk

	.adj_hrs_clk(io_in[10]),
	.adj_min_clk(io_in[11]),
	.adj_sec_clk(io_in[12]),
	
	                         
 // Combine 6-Pins VGA output --clk
 
        .hsync_clk(io_out[13]),               // to VGA Connector
        .vsync_clk(io_out[14]),               // to VGA Connector
        .rrggbb_clk(io_out[20:15]), 
         
//  Combine 12-Pin VGA outputs --box
              
        .hsync_box(io_out[22]),               // to VGA Connector
        .vsync_box(io_out[23]),               // to VGA Connector
        .rgb_box(io_out[35:24]), 
        
//   Verification Pins
               
   	.tick_1Hz(io_out[21])
   //	.tick_1Hz_2(io_out[37])	
 
    );  
    
     
endmodule 

`default_nettype wire


