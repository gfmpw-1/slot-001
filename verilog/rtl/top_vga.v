
// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/04/2023 02:15:55 PM
// Design Name: 
// Module Name: for Chip_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//
//      Total Inputs        			 5
//      Total Outputs       			14
//		Verification Output Pin			 2
//    --------------------------------------
//      Total Pins used     			21

module top_vga(
 
 //VGA Digital Clock
		
	input clk,                 
	input wire reset_n,
	input wire adj_hrs_clk,
	input wire adj_min_clk,
	input wire adj_sec_clk,
	    
	input wire reset_box,
	                         
 // Combine 6-Pin VGA output
 
        output hsync_clk,               // to VGA Connector
        output vsync_clk,               // to VGA Connector
        output [5:0] rrggbb_clk, 
        
 
 
//  Combine 12-Pin VGA outputs
              
        output hsync_box,               // to VGA Connector
        output vsync_box,               // to VGA Connector
        output [11:0] rgb_box, 
        
//   Verification Pins
               
	output tick_1Hz
	//output tick_1Hz_2	
 
    );  
    
   
    // create the 1Hz signal if clk is 100MHz
    reg [31:0] ctr_1Hz = 32'h0;
    reg r_1Hz = 1'b0;
    
  //  reg [31:0] ctr_1Hz_2 = 32'h0;
  //  reg r_1Hz_2 = 1'b0;
    
    
 //-------------------------------------------------------------------
 // 	Simple Clock On Output for Testing Purpose
 //		To Verify Precision

    
    always @(posedge clk or posedge reset_n)
        if(reset_n)
            ctr_1Hz <= 32'h0;
        else
            if(ctr_1Hz == 49_999_999) begin
                ctr_1Hz <= 32'h0;
                r_1Hz <= ~r_1Hz;
            end
            else
                ctr_1Hz <= ctr_1Hz + 1;
                
     assign tick_1Hz = r_1Hz; 
	 
// ********************************************************
/*
    always @(posedge clk or posedge reset_box)
        if(reset_box)
            ctr_1Hz_2 <= 32'h0;
        else
            if(ctr_1Hz_2 == 49_999_999) begin
                ctr_1Hz_2 <= 32'h0;
                r_1Hz_2 <= ~r_1Hz_2;
            end
            else
                ctr_1Hz_2 <= ctr_1Hz_2 + 1;
                
     assign tick_1Hz_2 = r_1Hz; 
*/     
// ********************************************************
     
     
   low_pins_vga low_pins_vga_inst(
    
	    .clk(clk), 
	    .reset_n(reset_n),
	    .adj_hrs(adj_hrs_clk),
	    .adj_min(adj_min_clk),
	    .adj_sec(adj_sec_clk),
	    
	    .hsync(hsync_clk),
	    .vsync(vsync_clk),
	    .rrggbb(rrggbb_clk)
	    
	    );

     
//      BOX VGA TEST ON Basys 
          
    box  top_box(
        .clk_100MHz(clk),              // 100MHz
        .reset(reset_box),            // btnR
        .hsync(hsync_box),            // to VGA Connector
        .vsync(vsync_box),            // to VGA Connector
        .rgb(rgb_box)       		// to DAC, to VGA Connector
         );
  
        
endmodule


