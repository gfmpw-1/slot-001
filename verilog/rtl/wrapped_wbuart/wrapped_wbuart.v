// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0

module wrapped_wbuart (

`ifdef USE_POWER_PINS
    inout vdd,	// User area 1 1.8V supply
    inout vss,	// User area 1 digital ground
`endif

    // Wishbone Slave ports (WB MI A)
    input wb_clk_i,
    input wb_rst_i,
    input wbs_stb_i,
    input wbs_cyc_i,
    input wbs_we_i,
    input [3:0] wbs_sel_i,
    input [31:0] wbs_dat_i,
    input [31:0] wbs_adr_i,
    output wbs_ack_o,
    output [31:0] wbs_dat_o,

       // IOs
    input  wire [1:0] io_in,
    output wire [1:0] io_out,
    output wire [1:0] io_oeb

);
   
 /*//		for indication		
		.i_cts_n(i_cts_n),
		.o_rts_n(o_rts_n),
		.o_uart_rx_int(o_uart_rx_int), 
		.o_uart_tx_int(o_uart_tx_int),
		.o_uart_rxfifo_int(o_uart_rxfifo_int), 
		.o_uart_txfifo_int(o_uart_txfifo_int)
		
	);
	
wire	o_rts_n,i_cts_n,o_uart_rx_int,o_uart_tx_int,o_uart_rxfifo_int,o_uart_txfifo_into_wb_stall;
	*/

   
  
wire wbs_ack_o_wbuart;
reg wbs_ack_o_reg;

reg [31:0] wbs_adr_i_wbuart, wbs_dat_o_reg;
wire [31:0] wbs_dat_o_wbuart;
 
 
    assign io_oeb[0]  = 1'b1;
    assign io_out[0]  = 1'b0;
    
    assign io_oeb[1]  = 1'b0;  
      
wbuart   wbuart (
   
     		.i_clk(wb_clk_i), 
   		.i_reset(wb_rst_i),
		
		// Wishbone inputs
		.i_wb_cyc(wbs_cyc_i),
		.i_wb_stb(wbs_stb_i), 
		.i_wb_we(wbs_we_i),
		.i_wb_addr(wbs_adr_i[10:9]),
		.i_wb_data(wbs_dat_i),
		.i_wb_sel(wbs_sel_i),
		.o_wb_stall(),
		.o_wb_ack(wbs_ack_o_wbuart),
		.o_wb_data(wbs_dat_o_wbuart),
		
//		used only two to pad
		.i_uart_rx(io_in[0]),
		.o_uart_tx(io_out[1]),
		
//		for indication		
		.i_cts_n(),
		.o_rts_n(),
		.o_uart_rx_int(), 
		.o_uart_tx_int(),
		.o_uart_rxfifo_int(), 
		.o_uart_txfifo_int()
		
	);
	
	
	reg [2:0] sel;
 always @* 
	begin 
		sel = wbs_adr_i[15:13]; 
	end

always@(posedge wb_clk_i )
begin
	if (wb_rst_i)
			begin 
			wbs_adr_i_wbuart<=0;
			wbs_dat_o_reg <= 0;
			wbs_ack_o_reg <= 0;
		end
else
	begin
		if(wbs_adr_i[15:13] == 3'b110) begin
			    wbs_adr_i_wbuart <= wbs_adr_i;
			    wbs_dat_o_reg <= wbs_dat_o_wbuart; 
			    wbs_ack_o_reg <= wbs_ack_o_wbuart;
			end
		else
				begin
							wbs_adr_i_wbuart<=0;	
							wbs_dat_o_reg <= 0;
							wbs_ack_o_reg <= 0;

				end
				
end
end
assign wbs_ack_o = wbs_ack_o_reg;
assign wbs_dat_o = wbs_dat_o_reg;


endmodule
 
 
