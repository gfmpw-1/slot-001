//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:20:22 11/19/2023 
// Design Name: 
// Module Name:    wrapped_aes 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module wrapped_aes(

`ifdef USE_POWER_PINS
    inout vdd,	// User area 1 1.8V supply
    inout vss,	// User area 1 digital ground
`endif

	input wire wb_clk_i,
	input wire wb_rst_i,
	  
	input wbs_stb_i,
	input wbs_cyc_i,
	input  wbs_we_i,
	input  [3:0] wbs_sel_i,
	input  [31:0] wbs_dat_i,
	input  [31:0] wbs_adr_i,
	output  wbs_ack_o,
	output [31:0] wbs_dat_o	
    );
    
    
reg wbs_ack_o_reg;
wire valid_aes;
reg [31:0] wbs_adr_i_aes, wbs_dat_o_reg;
wire [31:0] wbs_dat_o_aes;
  
 
 assign valid_aes = wbs_stb_i && wbs_cyc_i;

aes aes (


    .clk(wb_clk_i),
    .reset_n(wb_rst_i),  
    .cs(valid_aes),
    .we(wbs_we_i),
    .address(wbs_adr_i_aes[7:0]),
    .write_data(wbs_dat_i),
    .read_data(wbs_dat_o_aes)

    );


 reg [2:0] sel;
 always @* 
	begin 
		sel = wbs_adr_i[15:13]; 
	end

always@(posedge wb_clk_i )
begin
	if (wb_rst_i)
			begin 
			wbs_adr_i_aes<=0;
			wbs_dat_o_reg <= 0;
			wbs_ack_o_reg <= 0;
		end
else
	begin
		if(wbs_adr_i[15:13] == 3'b011) begin
			    wbs_adr_i_aes <= wbs_adr_i;
			    wbs_dat_o_reg <= wbs_dat_o_aes; 
			    wbs_ack_o_reg <= 1'b1;
			end
		else
				begin
							wbs_adr_i_aes<=0;	
							wbs_dat_o_reg <= 0;
							wbs_ack_o_reg <= 0;

				end
				
end
end

assign wbs_ack_o = wbs_ack_o_reg;
assign wbs_dat_o = wbs_dat_o_reg;


endmodule

