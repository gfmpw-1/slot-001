// SPDX-FileCopyrightText: 2020 Efabless Corporation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// SPDX-License-Identifier: Apache-2.0



module wrapped_xtea(

`ifdef USE_POWER_PINS
    inout vdd,	// User area 1 1.8V supply
    inout vss,	// User area 1 digital ground
`endif

	input wire wb_clk_i,
	input wire wb_rst_i,
	  
	input wbs_stb_i,
	input wbs_cyc_i,
	input  wbs_we_i,
	input  [3:0] wbs_sel_i,
	input  [31:0] wbs_dat_i,
	input  [31:0] wbs_adr_i,
	output  wbs_ack_o,
	output [31:0] wbs_dat_o	
    );
    
    
reg wbs_ack_o_reg;
wire valid_xtea;
reg [31:0] wbs_adr_i_xtea, wbs_dat_o_reg;
wire [31:0] wbs_dat_o_xtea;
  
 
 assign valid_xtea = wbs_stb_i && wbs_cyc_i;

xtea xtea (


    .clk(wb_clk_i),
    .reset_n(wb_rst_i),  
    .cs(valid_xtea),
    .we(wbs_we_i),
    .address(wbs_adr_i_xtea[7:0]),
    .write_data(wbs_dat_i),
    .read_data(wbs_dat_o_xtea)

    );


 reg [2:0] sel;
 always @* 
	begin 
		sel = wbs_adr_i[15:13]; 
	end

always@(posedge wb_clk_i )
begin
	if (wb_rst_i)
			begin 
			wbs_adr_i_xtea<=0;
			wbs_dat_o_reg <= 0;
			wbs_ack_o_reg <= 0;
		end
else
	begin
		if(wbs_adr_i[15:13] == 3'b001) begin
			    wbs_adr_i_xtea <= wbs_adr_i;
			    wbs_dat_o_reg <= wbs_dat_o_xtea; 
			    wbs_ack_o_reg <= 1'b1;
			end
		else
				begin
							wbs_adr_i_xtea<=0;	
							wbs_dat_o_reg <= 0;
							wbs_ack_o_reg <= 0;

				end
				
end
end
assign wbs_ack_o = wbs_ack_o_reg;
assign wbs_dat_o = wbs_dat_o_reg;

 
endmodule
 
